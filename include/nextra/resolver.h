
#ifndef NEX_RESOLVER_H
#define NEX_RESOLVER_H 1

#include <nebase/cdefs.h>
#include <nebase/evdp/types.h>

#include <stdbool.h>
#include <sys/socket.h>

#include <ares.h>

typedef struct nex_resolver* nex_resolver_t;
typedef struct nex_resolver_ctx* nex_resolver_ctx_t;

extern nex_resolver_t nex_resolver_create(struct ares_options *options, int optmask)
	_nattr_warn_unused_result _nattr_nonnull((1));
extern void nex_resolver_destroy(nex_resolver_t r)
	_nattr_nonnull((1));
/**
 * \param[in] addr port is not used, only family and addr should be set
 * \return -1 if invalid param, or 0 if set, but it doesn't mean the real bind will be success
 */
extern int nex_resolver_set_bind_ip(nex_resolver_t r, const struct sockaddr *addr)
	_nattr_warn_unused_result _nattr_nonnull((1, 2));

extern int nex_resolver_associate(nex_resolver_t r, neb_evdp_queue_t q)
	_nattr_warn_unused_result _nattr_nonnull((1, 2));
extern void nex_resolver_disassociate(nex_resolver_t r)
	_nattr_nonnull((1));

extern int nex_resolver_set_servers(nex_resolver_t r, struct ares_addr_port_node *servers)
	_nattr_warn_unused_result _nattr_nonnull((1, 2));


/**
 * resolver context, for each resolve action
 */

extern nex_resolver_ctx_t nex_resolver_new_ctx(nex_resolver_t r, void *udata)
	_nattr_warn_unused_result _nattr_nonnull((1));
extern void nex_resolver_del_ctx(nex_resolver_t r, nex_resolver_ctx_t c)
	_nattr_nonnull((1, 2));
extern bool nex_resolver_ctx_in_use(nex_resolver_ctx_t c)
	_nattr_nonnull((1));

/**
 * \param[in] name there is no IDN convertion in this function
 */
extern int nex_resolver_ctx_gethostbyname(nex_resolver_ctx_t c, const char *name, int family, ares_host_callback cb)
	_nattr_warn_unused_result _nattr_nonnull((1, 2, 4));
/**
 * \param[in] addr port is not used, only family and addr should be set
 */
extern int nex_resolver_ctx_gethostbyaddr(nex_resolver_ctx_t c, const struct sockaddr *addr, ares_host_callback cb)
	_nattr_warn_unused_result _nattr_nonnull((1, 2, 3));
extern int nex_resolver_ctx_send(nex_resolver_ctx_t c, const unsigned char *qbuf, int qlen, ares_callback cb)
	_nattr_warn_unused_result _nattr_nonnull((1, 2, 4));


/**
 * util functions
 */

extern struct ares_addr_port_node *nex_resolver_new_server(const char *s)
	_nattr_warn_unused_result _nattr_nonnull((1));
extern void nex_resolver_del_server(struct ares_addr_port_node *n)
	_nattr_nonnull((1));

/**
 * \param[in] len may be 0 if type is null terminated
 * \return DNS type value, as defined in \<arpa/nameser.h>
 */
extern int nex_resolver_parse_type(const char *type, int len)
	_nattr_nonnull((1)) _nattr_pure;

#endif
