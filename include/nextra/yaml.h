
#ifndef NEX_YAML_H
#define NEX_YAML_H 1

#include <nebase/cdefs.h>

#include <yaml.h>

struct nex_yaml_parser_ctx {
	yaml_parser_t *parser;
	const char *filename;
};

typedef int (*nex_yaml_map_each)(
	struct nex_yaml_parser_ctx *ctx,
	const yaml_char_t *key, const yaml_event_t *yep,
	void *udata);
typedef int (*nex_yaml_vec_each)(
	struct nex_yaml_parser_ctx *ctx,
	int index, const yaml_event_t *yep,
	void *udata);

extern int nex_yaml_parse_stream(struct nex_yaml_parser_ctx *ctx, nex_yaml_vec_each each_doc, void *udata)
	_nattr_warn_unused_result _nattr_nonnull((1, 2, 3));

extern int nex_yaml_parse_doc_as_map(struct nex_yaml_parser_ctx *ctx, nex_yaml_map_each each_kv, void *udata)
	_nattr_warn_unused_result _nattr_nonnull((1, 2, 3));
extern int nex_yaml_parse_doc_as_vec(struct nex_yaml_parser_ctx *ctx, nex_yaml_vec_each each_v, void *udata)
	_nattr_warn_unused_result _nattr_nonnull((1, 2, 3));

extern int nex_yaml_parse_map(struct nex_yaml_parser_ctx *ctx, nex_yaml_map_each each_kv, void *udata)
	_nattr_warn_unused_result _nattr_nonnull((1, 2, 3));
extern int nex_yaml_parse_vec(struct nex_yaml_parser_ctx *ctx, nex_yaml_vec_each each_v, void *udata)
	_nattr_warn_unused_result _nattr_nonnull((1, 2, 3));

extern int nex_yaml_skip_invalid_key(struct nex_yaml_parser_ctx *ctx, const yaml_char_t *key, const yaml_event_t *yep)
	_nattr_warn_unused_result _nattr_nonnull((1, 2, 3));

#endif
