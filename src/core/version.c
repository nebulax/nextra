
#include "version.h"

static const char nex_version[] = NEXTRA_VERSION_STRING;

const char *nex_version_str(void)
{
	return nex_version;
}

int nex_version_code(void)
{
	return NEXTRA_VERSON_CODE;
}
