
add_library(nextra_core SHARED
  version.c)
set_target_properties(nextra_core PROPERTIES
  EXPORT_NAME NeXtra::Core
  SOVERSION 0
  VERSION ${PROJECT_VERSION})
