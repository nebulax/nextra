
#include <nebase/syslog.h>

#include <nextra/yaml.h>

static void log_yaml_internal_error(struct nex_yaml_parser_ctx *ctx)
{
	yaml_parser_t *parser = ctx->parser;
	const char *error_type = "unknown";
	switch (parser->error) {
	case YAML_MEMORY_ERROR:
		neb_syslog(LOG_CRIT, "YAML Parser: Memory Error");
		return;
		break;
	case YAML_READER_ERROR:
		error_type = "Reader Error";
		break;
	case YAML_SCANNER_ERROR:
		error_type = "Scanner Error";
		break;
	case YAML_PARSER_ERROR:
		error_type = "Parser Error";
		break;
	default:
		neb_syslog(LOG_ERR, "YAML Parser: unknown error type %d", parser->error);
		return;
		break;
	}

	neb_syslog(LOG_ERR, "YAML %s: %s in %s:%lu:%lu %s in %s:%lu:%lu",
	           error_type, parser->problem, ctx->filename,
	           parser->problem_mark.line + 1, parser->problem_mark.column + 1,
	           parser->context, ctx->filename,
	           parser->context_mark.line + 1, parser->context_mark.column + 1);
}

static void nex_yaml_log_detail(int pri, struct nex_yaml_parser_ctx *ctx, const char *prefix)
{
	yaml_parser_t *parser = ctx->parser;
	neb_syslog(pri, "%s %s:%lu:%lu", prefix, ctx->filename, parser->mark.line, parser->mark.column);
}

int nex_yaml_parse_stream(struct nex_yaml_parser_ctx *ctx, nex_yaml_vec_each each_doc, void *udata)
{
	int ret = 0, done = 0, index = 0;

	while (!done) {
		yaml_event_t event;
		if (!yaml_parser_parse(ctx->parser, &event)) {
			log_yaml_internal_error(ctx);
			ret = -1;
			break;
		}

		switch (event.type) {
		case YAML_STREAM_START_EVENT:
			// may get encoding, but we don't care
			break;
		case YAML_STREAM_END_EVENT:
			done = 1;
			break;
		case YAML_DOCUMENT_START_EVENT:
			ret = each_doc(ctx, index, &event, udata);
			if (ret != 0) {
				nex_yaml_log_detail(LOG_ERR, ctx, "error parsing yaml document");
				done = 1;
			}
			index++;
			break;
		case YAML_DOCUMENT_END_EVENT:
		default:
			// NOTE allow early quit from each_doc with 0 as return value
			break;
		}

		yaml_event_delete(&event);
	}

	return ret;
}

int nex_yaml_parse_doc_as_map(struct nex_yaml_parser_ctx *ctx, nex_yaml_map_each each_kv, void *udata)
{
	int ret = 0;
	yaml_event_t event;

	if (!yaml_parser_parse(ctx->parser, &event)) {
		log_yaml_internal_error(ctx);
		return -1;
	}
	if (event.type != YAML_MAPPING_START_EVENT) {
		nex_yaml_log_detail(LOG_ERR, ctx, "yaml document content type mismatch, expect mapping");
		ret = -1;
		goto exit;
	}

	yaml_event_delete(&event);
	ret = nex_yaml_parse_map(ctx, each_kv, udata);
	if (ret != 0)
		return ret;

	if (!yaml_parser_parse(ctx->parser, &event)) {
		log_yaml_internal_error(ctx);
		return -1;
	}
	if (event.type != YAML_DOCUMENT_END_EVENT) {
		nex_yaml_log_detail(LOG_ERR, ctx, "yaml document end expected here");
		ret = -1;
	}

exit:
	yaml_event_delete(&event);
	return ret;
}

int nex_yaml_parse_doc_as_vec(struct nex_yaml_parser_ctx *ctx, nex_yaml_vec_each each_v, void *udata)
{
	int ret = 0;
	yaml_event_t event;

	if (!yaml_parser_parse(ctx->parser, &event)) {
		log_yaml_internal_error(ctx);
		return -1;
	}
	if (event.type != YAML_SEQUENCE_START_EVENT) {
		nex_yaml_log_detail(LOG_ERR, ctx, "yaml document content type mismatch, expect sequence");
		ret = -1;
		goto exit;
	}

	yaml_event_delete(&event);
	ret = nex_yaml_parse_vec(ctx, each_v, udata);
	if (ret != 0)
		return ret;

	if (!yaml_parser_parse(ctx->parser, &event)) {
		log_yaml_internal_error(ctx);
		return -1;
	}
	if (event.type != YAML_DOCUMENT_END_EVENT) {
		nex_yaml_log_detail(LOG_ERR, ctx, "yaml document end expected here");
		ret = -1;
	}

exit:
	yaml_event_delete(&event);
	return ret;
}

int nex_yaml_parse_map(struct nex_yaml_parser_ctx *ctx, nex_yaml_map_each each_kv, void *udata)
{
	int ret = 0;

	yaml_char_t *key = NULL;
	for (;;) {
		yaml_event_t event;
		if (!yaml_parser_parse(ctx->parser, &event)) {
			log_yaml_internal_error(ctx);
			ret = -1;
			break;
		}

		if (event.type == YAML_MAPPING_END_EVENT) {
			yaml_event_delete(&event);
			break;
		}

		int is_key = ctx->parser->simple_key_allowed;
		if (!is_key) {
			if (event.type != YAML_SCALAR_EVENT) {
				nex_yaml_log_detail(LOG_ERR, ctx, "only scalar key type is supported");
				yaml_event_delete(&event);
				ret = -1;
				break;
			}
			if (key)
				free(key);
			key = (yaml_char_t *)strdup((const char *)event.data.scalar.value);
			if (!key) {
				neb_syslogl(LOG_ERR, "strdup: %m");
				yaml_event_delete(&event);
				ret = -1;
				break;
			}
		} else {
			if (each_kv(ctx, key, &event, udata) != 0) {
				nex_yaml_log_detail(LOG_ERR, ctx, "error parsing yaml mapping");
				yaml_event_delete(&event);
				ret = -1;
				break;
			}
		}

		yaml_event_delete(&event);
	}
	if (key)
		free(key);

	return ret;
}

int nex_yaml_parse_vec(struct nex_yaml_parser_ctx *ctx, nex_yaml_vec_each each_v, void *udata)
{
	int ret = 0, index = 0;

	for (;;) {
		yaml_event_t event;
		if (!yaml_parser_parse(ctx->parser, &event)) {
			log_yaml_internal_error(ctx);
			ret = -1;
			break;
		}

		if (event.type == YAML_SEQUENCE_END_EVENT) {
			yaml_event_delete(&event);
			break;
		}

		if (each_v(ctx, index, &event, udata) != 0) {
			nex_yaml_log_detail(LOG_ERR, ctx, "error parsing yaml sequence");
			yaml_event_delete(&event);
			ret = -1;
			break;
		}
		index++;

		yaml_event_delete(&event);
	}

	return ret;
}

static int nex_yaml_skip_until(struct nex_yaml_parser_ctx *ctx, yaml_event_type_t type, int indent)
{
	int done = 0;
	do {
		yaml_event_t event;
		if (!yaml_parser_parse(ctx->parser, &event)) {
			log_yaml_internal_error(ctx);
			return -1;
		}

		yaml_event_type_t event_type = event.type;
		yaml_event_delete(&event);

		if (ctx->parser->indent == indent && event_type == type)
			return 0;

		switch (event_type) {
		case YAML_STREAM_END_EVENT:
		case YAML_DOCUMENT_END_EVENT:
			done = 1;
			break;
		default:
			break;
		}
	} while (!done);

	neb_syslog(LOG_ERR, "no maching yml event %d found at indent %d in file %s", type, indent, ctx->filename);
	return -1;
}

int nex_yaml_skip_invalid_key(struct nex_yaml_parser_ctx *ctx, const yaml_char_t *key, const yaml_event_t *yep)
{
	yaml_parser_t *parser = ctx->parser;
	neb_syslog(LOG_WARNING, "invalid key %s found at %s:%lu:%lu, skip it",
	           key, ctx->filename, parser->mark.line, parser->mark.column);
	int ret = 0;
	switch (yep->type) {
	case YAML_SCALAR_EVENT:
		break;
	case YAML_MAPPING_START_EVENT:
		ret = nex_yaml_skip_until(ctx, YAML_MAPPING_END_EVENT, parser->indent);
		break;
	case YAML_SEQUENCE_START_EVENT:
		ret = nex_yaml_skip_until(ctx, YAML_SEQUENCE_END_EVENT, parser->indent);
		break;
	default:
		neb_syslog(LOG_ERR, "invalid value type %d for key %s", yep->type, key);
		ret = -1;
		break;
	}
	if (ret != 0)
		neb_syslog(LOG_ERR, "unable to skip invalid key %s", key);
	return ret;
}
