
set(WITH_CARES_DESC "Build with c-ares")
option(WITH_CARES ${WITH_CARES_DESC} ON)
add_feature_info(WITH_CARES WITH_CARES ${WITH_CARES_DESC})

set(WITH_YAML_DESC "Build with YAML")
option(WITH_YAML ${WITH_YAML_DESC} ON)
add_feature_info(WITH_YAML WITH_YAML ${WITH_YAML_DESC})
